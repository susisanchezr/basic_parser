package com.example.basicparser


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {

//TODO: call parse function with a song name
    private fun parse()
    {
        val context = this.applicationContext
        //TODO: get ressources from song name
        val inputStream = context.resources.openRawResource(R.raw.song1)
        val inputreader = InputStreamReader(inputStream)
        val buffreader = BufferedReader(inputreader)
        var line: String?


        val song = Song()

            while (buffreader.readLine().also { line = it } != null) {

                val spl = line!!.split(":")
                if (spl[0]=="[ti") {
                    song.name=spl[1].split("]")[0]
                } else if (spl[0]=="[ar") {
                    song.artist=spl[1].split("]")[0]
                } else if (spl[0]=="[pitch") {
                    song.pitch=spl[1].split("]")[0].toInt()
                } else if (spl[0]=="[speed") {
                    song.speed=spl[1].split("]")[0].toInt()
                } else if (spl[0]=="[offset") {
                    song.offset=spl[1].split("]")[0].toInt()
                } else {
                    if (line != "") {
                        val spl = line!!.split("]")
                        var sentence = spl[1]
                        if (sentence != "") {
                            // sentence starts with time (ex: [00:04.00])
                            val dateString = spl[0].replace("[", "")
                            // a lyric is a sentence of the song
                            val lyric = Lyric()
                            lyric.dateString = dateString
                            lyric.sentence = sentence

                            if ("#human" in sentence){
                                val splSentence = sentence!!.split("#human")
                                lyric.human = true
                                sentence = splSentence[1]
                            }
                            else if ("#pepper" in sentence)
                            {
                                val splSentence = sentence!!.split("#pepper")
                                lyric.human = false
                                sentence = splSentence[1]
                            }

                            if ("\\rspd" in sentence)
                            {
                                sentence = sentence!!.split("\\rspd=")[1]

                                var splRspd = sentence.split("\\")

                                lyric.speed = splRspd[0].toInt()
                                sentence = splRspd[1]
                            } else
                            {
                                lyric.speed = song.speed
                            }

                            if ("\\vct" in sentence)
                            {
                                sentence = sentence!!.split("\\vct=")[1]

                                var splVct = sentence.split("\\")

                                lyric.pitch = splVct[0].toInt()
                                sentence = splVct[1]
                            } else
                            {
                                lyric.pitch = song.pitch
                            }

                            lyric.sentence = sentence
                            song.lyricList.add((lyric))
                        }
                    }
                }
            }
            song.showSong()

        //val dir = this.getDir("output_folder", Context.MODE_PRIVATE)
        //val fileName = "output.top"
        //val fileContent = "blablabla"
        //File(dir.absolutePath+"/"+fileName).writeText(fileContent)

    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {

            parse()

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the men²u; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}

