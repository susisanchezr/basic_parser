package com.example.basicparser
import android.util.Log

class Song {
    // a song is a list of Lyrics (sentences)
    internal val lyricList: MutableList<Lyric> = ArrayList()
    internal var name: String? = null
    internal var artist: String? = null
    internal var offset: Int = 0
    internal var speed: Int = 0
    internal var pitch: Int = 0


    fun showSong(){
        Log.d("META","NAME:"+this.name)
        Log.d("META","ARTIST:"+this.artist)
        Log.d("META","OFFSET:"+this.offset)
        Log.d("META","SPEED:"+this.speed)
        Log.d("META","PITCH:"+this.pitch)
        for (e in this.lyricList) {
            Log.i("LYRIC", e.getLyric())
        }
    }


}

// a sentence of a song
class Lyric {
    internal var sentence: String? = null
    internal var dateString: String? = null
    // bool variable to know if it is a human or Pepper that sings
    internal var human: Boolean = false
    // keep speed and pitch value of each sentence in case they change from default
    internal var speed: Int = 0
    internal var pitch: Int = 0

    fun getLyric(): String?{
        return "[H:"+this.human+","+this.speed+","+this.pitch+"]"  + this.sentence
    }
}

